from django.http import HttpResponse
from django.shortcuts import render
from home.models import Student


def show_all_students(request):
    """
    Show all students in template
    """
    # выбрать всех студентов из таблицы students(джанго сама
    # дописывает `s` в окончании таблицы, но модели всегда должны
    # быть в единственном числе)
    students = Student.objects.all()

    # для рендеринга темплейта нужно использовать render
    # эта функция принимает минимум два параметра request, template_name
    return render(
        request=request,
        template_name='index.html',
        context={
            'students': students,
        }
    )


def create_student(request):
    """
    Create student by student's name
    """
    # все GET параметры хранятся в request.GET
    student_name_from_request = request.GET.get('name')

    if not student_name_from_request:
        return HttpResponse('Student name missing')

    # Чтобы создать нового юзера нужно инициализировать Модель
    student = Student()
    # далее сохраняем параметры в объект модели
    student.name = student_name_from_request
    # сохранение
    student.save()

    return HttpResponse('Student {} have been created'.format(student.name))
